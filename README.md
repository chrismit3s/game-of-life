# Conway's Game of Life

Conway's famous Game of Life.

![glider gun](./out/glider-gun.webm)

## Setup

Just clone this repo, create a venv, install the packages and run it.

On linux:

    sudo apt update && sudo apt install python3.8-dev libsdl-ttf2.0-dev libsdl-mixer1.2-dev libsdl-image1.2-dev  # pygame deps
    git clone https://gitlab.com/chrismit3s/game-of-life
    cd game-of-life
    python3.8 -m venv venv
    source ./venv/bin/activate
    python -m pip install -r requirements.txt

On windows (with [py launcher](https://docs.python.org/3/using/windows.html) installed):

    git clone https://gitlab.com/chrismit3s/game-of-life
    cd game-of-life
    py -3.8 -m venv venv
    .\venv\scripts\activate.bat
    python -m pip install -r requirements.txt

Then just run the `src` module with:

    python -m src [-R RULES]

`-R`/`--rules` sets the rules for the game of life (when cells die/are born), the string must be either in [S/B or in B/S notation](https://www.conwaylife.com/wiki/Rulestring).
