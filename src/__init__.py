SCALE_INIT = 100.0
SCALE_FACTOR = 0.9

# see coolors.co/0a141f-f5b700-0eb1d2-587674-f1fffa
PALETTE = {
    "empty": (0x0A, 0x14, 0x1F),
    #"empty":      (0x58, 0x76, 0x74),
    "cell":       (0xF5, 0xB7, 0x00)}



from src.infinitegrid import InfiniteGrid
from src.game import Game
