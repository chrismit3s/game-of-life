from itertools import product
from pygame import draw
from pygame import gfxdraw
from src import InfiniteGrid
from src import SCALE_INIT, SCALE_FACTOR, PALETTE
from time import sleep
import logging as log
import numpy as np
import pygame as pg


def _iter_indices(start, end):
    outer, inner = zip(start, end)
    for x in range(*sorted(outer)):
        for y in range(*sorted(inner)):
            yield np.array((x, y), dtype="int")


class Game():
    def __init__(self, res, fps, rules=None):
        # initialize pygame (repeated calls have no effect)
        pg.init()

        # block uninteresting events
        pg.event.set_blocked([pg.ACTIVEEVENT, 
                              pg.JOYAXISMOTION, 
                              pg.JOYBALLMOTION, 
                              pg.JOYHATMOTION, 
                              pg.JOYBUTTONUP, 
                              pg.JOYBUTTONDOWN, 
                              pg.VIDEOEXPOSE, 
                              pg.USEREVENT])

        # display related
        self.fps = fps
        self.res = np.array(res)
        self.screen = pg.display.set_mode(res, pg.RESIZABLE)
        self.clock = pg.time.Clock()

        # for moving the screen, zooming, etc
        self.offset = self.res / 2
        self.scale = SCALE_INIT

        #self.rules = rules or {"birth": (3,), "survival": (2, 3)}  # the classic rules (first list: birth rules, second list: survival rules)
        #self.rules = rules or {"birth": (1, 3, 5, 7), "survival": (0, 2, 4, 6, 8)}
        self.rules = rules or {"birth": (3, 6, 7, 8), "survival": (3, 4, 6, 7, 8)}
        self.grid = InfiniteGrid(dtype="uint8")
        self.dragging = False
        self.running = False
    
    def to_px(self, pos):
        """ from coordinates to pixel indices """
        return (pos * self.scale + self.offset).astype("int")

    def from_px(self, px):
        """ from pixel indices to coordinates """
        return (px - self.offset).astype("float") / self.scale

    def draw_circle(self, pos, r, color, filled=False):
        """
        draws a line around the coordinates pos with radius r (in units, not px)
        """
        px = self.to_px(pos)
        r *= self.scale
        color = PALETTE[color] if type(color) == str else color

        # for normal size cirlces, use antialiased gfx version
        if min(self.res) > r:
            args = (self.screen, *px, int(r), color)

            gfxdraw.aacircle(*args)
            if filled:
                gfxdraw.filled_circle(*args)

        # if it gets too big, gfxdraw acts out (see stackoverflow.com/q/5001070), then use the normal circle
        else:
            draw.circle(self.screen,
                        color,
                        px,
                        int(r),
                        (0 if filled else 1))  # width

    def draw_line(self, p1, p2, color):
        color = PALETTE[color] if type(color) == str else color
        draw.aaline(self.screen, color, self.to_px(p1), self.to_px(p2))

    def draw(self):
        self.screen.fill(PALETTE["empty"])

        fx, fy = start = np.round(self.from_px((0, 0)) - 1).astype("int")
        tx, ty = end = np.round(self.from_px(self.res) + 1).astype("int")

        log.debug(f"{fx=} {fy=} {tx=} {ty=}")
        if (self.grid[fx:tx, fy:ty] == 0).all():
            log.info("bliting empty array to screen")


        colors = np.array((PALETTE["empty"], PALETTE["cell"]), dtype="uint8")
        surf = pg.surfarray.make_surface(colors[self.grid[fx:tx, fy:ty]])
        surf = pg.transform.scale(surf, (np.array(surf.get_size()) * self.scale).astype("int"))
        self.screen.blit(surf, self.to_px(start))

        pg.display.flip()

    def zoom(self, px, dir):
        """
        zooms the canvas towards the position px on screen; dir > 0 zooms in, dir < 0
        zooms out
        """
        factor = 1
        if dir > 0:
            factor = SCALE_FACTOR
        elif dir < 0:
            factor = 1 / SCALE_FACTOR

        self.scale *= factor
        self.offset = (self.offset - px) * factor + px

    def left_click(self, px):
        index = self.from_px(px)
        closest = np.floor(index).astype("int")
        self.grid[closest] = 1 - self.grid[closest]

    def tick_cell(self, index):
        index = np.array(index)

        # count the neighbors
        neighbors = 0
        for shift in product([-1, 0, 1], repeat=2):
            if any(shift):  # skip shift (0, 0) as it's the cell itself
                neighbors += self.grid[tuple(index + shift)] % 2  # %2 as we only look at the 1st bit (the 2nd bit is temporary storage of the current gen)

        # check if the number of neighbors allow the cell to survive/be born
        if neighbors in self.rules["birth" if self.grid[index] == 0 else "survival"]:
            self.grid[index] += 2

    def tick_grid(self):
        # find all cells that could be affected this gen
        indices = set()
        for index in self.grid.find(1):
            # adds index and the entire neighborhood to indices
            for shift in product([-1, 0, 1], repeat=2):
                indices.add(tuple(index + shift))

        # tick cell sets the 2nd bit to the next state of the cell
        for index in indices:
            self.tick_cell(index)
        self.grid //= 2

    def tick(self):
        done = False

        # check event queue
        for event in pg.event.get():
            # quit
            if event.type == pg.QUIT:
                done = True

            # window resized
            elif event.type == pg.VIDEORESIZE:
                self.res = np.array(event.size, dtype="int")
                self.screen = pg.display.set_mode(event.size, pg.RESIZABLE)

            # key pressed
            elif event.type == pg.KEYDOWN:
                # check for q or ctrl c
                if event.unicode == "q" or (event.key == pg.K_c and pg.key.get_mods() & pg.KMOD_CTRL):
                    done = True

                # t ticks one gen forwards
                if event.unicode == "t":
                    self.tick_grid()

                # space pauses/unpauses
                if event.key == pg.K_SPACE:
                    self.running = not self.running

            # mouse click
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == pg.BUTTON_LEFT:
                self.left_click(np.array(event.pos, dtype="float"))

            # mouse scroll zooms
            elif event.type == pg.MOUSEBUTTONDOWN and event.button in {pg.BUTTON_WHEELUP, pg.BUTTON_WHEELDOWN}:
                self.zoom(np.array(event.pos, dtype="float"),
                          1 if event.button == pg.BUTTON_WHEELUP else -1)
                    
            # holding right click drags the canvas
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == pg.BUTTON_RIGHT:
                self.dragging = True

            # right mouse button released => stop dragging
            elif event.type == pg.MOUSEBUTTONUP and event.button == pg.BUTTON_RIGHT:
                self.dragging = False
        
            # mouse moved => move canvas, if dragging
            elif event.type == pg.MOUSEMOTION and self.dragging:
                self.offset += event.rel

        # render
        self.draw()

        # tick grid
        if self.running:
            self.tick_grid()

        return done

    def loop(self):
        """
        the main loop; ticks forward until closed
        """
        while not self.tick():
            self.clock.tick(self.fps)
