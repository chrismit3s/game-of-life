from src import Game
from cProfile import run
from time import strftime
from pstats import Stats
from argparse import ArgumentParser
import logging as log


def parse_rule_string(rulestr):
    a, b = rulestr.split("/")

    # see https://www.conwaylife.com/wiki/Rulestring
    if all(c in "012345678" for c in a + b):  # all digits => S/B notation
        survival = a
        birth = b
    else:  # B/S notation
        birth = birth[1:]
        survival = survival[1:]

    return {"birth": tuple(int(c) for c in birth),
            "survival": tuple(int(c) for c in survival)}


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-R", "--rules",
                        default="23/3",
                        help="Set the rules for the game of life (when cell die/are born)")
    parser.add_argument("-P", "--profile",
                        action="store_true",
                        required=False,
                        help="Enables performance profiling")
    parser.add_argument("-L", "--log-level",
                        choices={"debug", "info", "warn", "error", "critical"},
                        default="error",
                        help="Configure log level")
    args = parser.parse_args()

    log.basicConfig(format="{relativeCreated:>10.0f} - {levelname:<8} - {module}:{funcName} - {message}",  style="{", level=args.log_level.upper())

    rules = parse_rule_string(args.rules)
    if args.profile:
        stats_file = f"./perf/stats-{strftime('%Y%m%dT%H%M%S')}.pstats"
        run("Game((1280, 720), 60, rules=rules).loop()", filename=stats_file)
        stats = Stats(stats_file)
        stats.sort_stats("cumtime").print_stats("src")
    else:
        Game((1280, 720), 60, rules=rules).loop()
